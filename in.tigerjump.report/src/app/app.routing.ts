

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './components/app/app.component';
const routes: Routes = [
    { path: '', component: AppComponent },
];

export const routing = RouterModule.forRoot(routes);
