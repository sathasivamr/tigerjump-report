import { DossCrmUiPage } from './app.po';

describe('doss-crm-ui App', function() {
  let page: DossCrmUiPage;

  beforeEach(() => {
    page = new DossCrmUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
